/*
	you can directly pass data into the function. The function can then call or use that data 

	

	function fucntionName(parameter){
		codeblock
	}
	fucntionName(argument)


	"name" is called parameter
	"paratmeter" acts as a named variable or container that exists onyl inside of a function
	it is used to store information that is provided to a function when it is called or invoked
*/

function printName(name){
	console.log("My name is: "+name);
}
printName("Juana");
printName("pat");


// variables can also be passed as an argument


// using multiple parameter

function createFullName(firstName, middleName, lastName){
	console.log(firstName+" "+middleName+" "+lastName);
}
createFullName("patrick", "miranda", "abad");




function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("the remainde of " + num + " divisible by 8 is" + remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is "+num+" divisible by 8?");
	console.log	(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);



/*
	Mini-Activity:
		1. Create a fucntion which is able to receive as an argument.
			-this function should be able to receive the name of your fav superhero
			-display the name of your favorite superhero in the console.
*/



function printHero(heroName){
	console.log("My fav hero name is: "+heroName);
}
printName("Deadpool");

// return statement
/*
	the return statement allows us to output a value form a function to be passed to the line or block of code that invoked or called the function
*/


function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName
	console.log("Can we print this message?");

}
// wont print anything:
	// returnFullName("pat", "miranda", "abad");
let completeName = returnFullName("John", "doe", "smith");
console.log(completeName);

/*
	wont print execute:
		console.log("Can we print this message?");

		because it wont execute the code after the return statement
*/

console.log(returnFullName("pat", "mi", "abad"));




function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress;
}

let myAddress = returnAddress("Sta. Mesa", "PH");
console.log(myAddress);



/*

	this will print but it will print "undefined" after "Immortal" because printPlayerInformation returns nothing. It only logs the message in the console.

	You cannot save any  value from printPlayerInformation because it does not RETURN anything
*/


function printPlayerInformation(userName, level, job){
	console.log("Username :" + userName);
	console.log("Level: " + level);
	console.log	("Job: " + job);
}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1);


/*
	#############################################
*/

function printPlayerInformation1(userName1, level1, job1){
	let playerInfo = "Username: "+userName1+"\nLevel: "+level1+"\nJob: "+job1;
	return playerInfo;
}

let pritntInfo = printPlayerInformation1("cardo123", "999", "Immortal");
console.log(pritntInfo);





/*
	#############################################
*/






