function printAdd(num1, num2){
	console.log("Displayed sum of: "+num1+" and "+num2);
	console.log(num1+num2);
}
printAdd(5, 15);


function printSub(num1, num2){
	console.log("Displayed difference of: "+num1+" and "+num2);
	console.log(num1-num2);
}
printSub(20, 5);


function returnProduct(num1, num2){
	return "Displayed product of: "+num1+" and "+num2+":\n"+num1*num2;
}
let product = returnProduct(50, 10);
console.log(product);

function returnQuotient(num1, num2){
	return "Displayed quotient of: "+num1+" and "+num2+":\n"+num1/num2;
}
let quotient = returnQuotient(50, 10);
console.log(quotient);

function returnCircleArea(num1){
	return "The reuslt of getting the are of a circle with "+num1+" radius: \n"+(3.1416*num1*num1);
}
let circleArea = returnCircleArea(15);
console.log(circleArea);

function returnAverage(num1, num2, num3, num4){
	return "The average of "+num1+" "+num2+" "+num3+" and "+num4+":\n"+((num1+num2+num3+num4)/4);
}
let averageVar = returnAverage(20,40,60,80);
console.log(averageVar);

function returnResult(num1, num2){
	let res = "Is "+num1+"/"+num2+" is a passing score?";
	return console.log(res+"\n"+(((num1/num2)*100) >=  75))
}
let result = returnResult(38,50);